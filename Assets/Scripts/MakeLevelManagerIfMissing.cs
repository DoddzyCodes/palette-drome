﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeLevelManagerIfMissing : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		if(LevelManager.Instance == null)
        {
            GameObject go = new GameObject("Level Manager");
            go.AddComponent<LevelManager>();
			Debug.Log("Created level manager");
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
