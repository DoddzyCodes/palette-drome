using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailFollower : MonoBehaviour {
    public float TrailTime = 2;

    private Vector3 targetPosition;
    private Color targetColor;

    private TrailRenderer trailRenderer;
  

    void Awake()
    {
        trailRenderer = GetComponent<TrailRenderer>();
    }
    public void SetColor(LevelManager.GameColor c)
    {
        trailRenderer.startColor = LevelManager.Instance.DisplayColor[c];
        trailRenderer.time = TrailTime;
    }

    private void UpdateLineRenderer()
    {
        throw new NotImplementedException();
    }

    public void GoTo(Vector3 position, LevelManager.GameColor c)
    {
        targetPosition = position;
        targetPosition.z = -2.0f;
        targetColor = LevelManager.Instance.DisplayColor[c];
        
        trailRenderer.endColor = trailRenderer.startColor;
        StartCoroutine(DoTraversal(TrailTime));
    }

    private IEnumerator DoTraversal(float time)
    {
        Vector3 offset = targetPosition - transform.position;
        Vector3 direction = offset.normalized;
        float distance = offset.magnitude;
        float distancePerSecond = distance / time;

        float twentyPercent = distance * 0.1f;

        transform.position += (twentyPercent * direction);
        float remainingDistance = distance - (2*twentyPercent);
        while (remainingDistance > 0)
        {
            float frameTravelDistance = distancePerSecond * Time.deltaTime;
            transform.position += direction * frameTravelDistance;
            remainingDistance -= frameTravelDistance;

            trailRenderer.startColor = Color.Lerp(targetColor, trailRenderer.endColor, remainingDistance / distance);
            yield return new WaitForEndOfFrame();
        }
    }
}
