﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwirlController : MonoBehaviour {
    public float FadeTime = 1.0f;
  
	public void FadeTo(Vector3 position)
    {
        position.z = 0.2f;
        StartCoroutine(DoFade(position));
    }

    private IEnumerator DoFade(Vector3 position)
    {
        float alpha = 1.0f;
        while(alpha > 0)
        {
            alpha -= (Time.deltaTime / FadeTime);
            SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer r in renderers)
            {
                Color c = r.color;
                c.a = alpha;
                r.color = c;
            }

            yield return new WaitForEndOfFrame();
        }


        transform.position = position;

        alpha = 0.0f;
        while (alpha < 1)
        {
            alpha += (Time.deltaTime / FadeTime);
            SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer r in renderers)
            {
                Color c = r.color;
                c.a = alpha;
                r.color = c;
            }

            yield return new WaitForEndOfFrame();
        }
    }
}
