﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour
{
    public static MusicManager Instance
    {
        get
        {
            if (_instance == null)
            {
                return null;
            }
            return _instance;
        }
    }

    private static MusicManager _instance;

//    public AudioClip ambientBirds;
//    public AudioClip ambientMelody;

    public float minWait = 0.1f;
    public float maxWait = 5f;
    public float minPitch = 0.45f;
    public float maxPitch = 1.05f;

    public AudioClip redLowNote;
    public AudioClip orangeLowNote;
    public AudioClip yellowLowNote;
    public AudioClip greenLowNote;
    public AudioClip blueLowNote;
    public AudioClip purpleLowNote;
    public AudioClip blackLowNote;
    public AudioClip redNote;
    public AudioClip orangeNote;
    public AudioClip yellowNote;
    public AudioClip greenNote;
    public AudioClip blueNote;
    public AudioClip purpleNote;
    public AudioClip blackNote;
    public AudioClip redHighNote;
    public AudioClip orangeHighNote;
    public AudioClip yellowHighNote;
    public AudioClip greenHighNote;
    public AudioClip blueHighNote;
    public AudioClip purpleHighNote;
    public AudioClip blackHighNote;

    [HideInInspector] public AudioSource audioSource;
    [HideInInspector] public AudioSource audioSource2;
    [HideInInspector] public AudioSource audioSource3;
    private float initialVol;
    private List<AudioClip> notes;
    public List<bool> enabledNotes;

    private void Awake()
    {
        _instance = this;
    }

    // Use this for initialization
    private void Start()
    {
        audioSource = GetComponents<AudioSource>()[0];
        audioSource2 = GetComponents<AudioSource>()[1];
        audioSource3 = GetComponents<AudioSource>()[2];
        initialVol = audioSource.volume;
        notes = new List<AudioClip>();
        notes.Add(redLowNote);
        notes.Add(orangeLowNote);
        notes.Add(yellowLowNote);
        notes.Add(greenLowNote);
        notes.Add(blueLowNote);
        notes.Add(purpleLowNote);
        notes.Add(blackLowNote);
        notes.Add(redNote);
        notes.Add(orangeNote);
        notes.Add(yellowNote);
        notes.Add(greenNote);
        notes.Add(blueNote);
        notes.Add(purpleNote);
        notes.Add(blackNote);
        notes.Add(redHighNote);
        notes.Add(orangeHighNote);
        notes.Add(yellowHighNote);
        notes.Add(greenHighNote);
        notes.Add(blueHighNote);
        notes.Add(purpleHighNote);
        notes.Add(blackHighNote);
        enabledNotes = new List<bool>();
        for (int i = 0; i < notes.Count; i++)
        {
            enabledNotes.Add(false);
        }
        StartCoroutine(randomMelody());
    }
    
    public IEnumerator FadeOutAudio()
    {
        float timePassed = 0;
        float timeGap = 0.1f;
        float totalTime = 4f;
        float volDiff1 = audioSource.volume / totalTime / timeGap;
        float volDiff2 = audioSource2.volume / totalTime / timeGap;
        float volDiff3 = audioSource3.volume / totalTime / timeGap;
        while (timePassed < 4f)
        {
            audioSource.volume -= 0.01f;
            audioSource.volume -= 0.01f;
            audioSource.volume -= 0.01f;
            yield return new WaitForSecondsRealtime(timeGap);
            timePassed += timeGap;
        }
    }

    private IEnumerator randomMelody()
    {
        int prevIndex = Random.Range(0, notes.Count);
        int index = prevIndex;
        List<AudioClip> availableNotes = new List<AudioClip>();

        yield return new WaitForSecondsRealtime(1f);

        float randPitch = Random.Range(minPitch, maxPitch);
        audioSource.pitch = randPitch;
        audioSource3.pitch = randPitch;

        while (true)
        {
            //slightly randomise volume
            audioSource.volume = initialVol + Random.Range(-0.1f, 0.05f) * initialVol;
//            audioSource.panStereo = Random.Range(-1f, 1f);
//            audioSource.pitch += Random.Range(0,1) * Random.Range(-0.003f, 0.003f);

            if (LevelManager.Instance.isColorCompleted[LevelManager.GameColor.Red])
            {
                Debug.Log("enabled Red");
                enabledNotes[0] = true;
                enabledNotes[7] = true;
                enabledNotes[14] = true;
            }
            if (LevelManager.Instance.isColorCompleted[LevelManager.GameColor.Orange])
            {
                Debug.Log("enabled Orange");
                enabledNotes[1] = true;
                enabledNotes[8] = true;
                enabledNotes[15] = true;
            }
            if (LevelManager.Instance.isColorCompleted[LevelManager.GameColor.Yellow])
            {
                Debug.Log("enabled Yellow");
                enabledNotes[2] = true;
                enabledNotes[9] = true;
                enabledNotes[16] = true;
            }
            if (LevelManager.Instance.isColorCompleted[LevelManager.GameColor.Green])
            {
                Debug.Log("enabled Green");
                enabledNotes[3] = true;
                enabledNotes[10] = true;
                enabledNotes[17] = true;
            }
            if (LevelManager.Instance.isColorCompleted[LevelManager.GameColor.Blue])
            {
                Debug.Log("enabled Blue");
                enabledNotes[4] = true;
                enabledNotes[11] = true;
                enabledNotes[18] = true;
            }
            if (LevelManager.Instance.isColorCompleted[LevelManager.GameColor.Purple])
            {
                Debug.Log("enabled Purple");
                enabledNotes[5] = true;
                enabledNotes[12] = true;
                enabledNotes[19] = true;
            }
            if (LevelManager.Instance.isColorCompleted[LevelManager.GameColor.Black])
            {
                Debug.Log("enabled Black");
                enabledNotes[6] = true;
                enabledNotes[13] = true;
                enabledNotes[20] = true;
            }

            availableNotes.Clear();
            int k = 0;
            foreach (var bl in enabledNotes)
            {
                if (bl)
                {
                    availableNotes.Add(notes[k]);
                }
                k++;
            }
            if (availableNotes.Count == 0)
            {
                yield break;
            }

            int rand = Random.Range(0, 100);
            //step up one
            if (rand < 25)
            {
                index = prevIndex + 1;
            }
            //step down one
            else if (rand < 40)
            {
                index = prevIndex - 1;
            }
            //same
            else if (rand < 65)
            {
                index = prevIndex;
            }
            //step down two
            else if (rand < 70)
            {
                index = prevIndex - 2;
            }
            //step up two
            else if (rand < 75)
            {
                index = prevIndex + 2;
            }
            //otherwise random
            else
            {
                index = Random.Range(0, notes.Count);
            }

            //make sure index is in valid range
            if (index >= notes.Count)
            {
                index = 0;
            }
            else if (index < 0)
            {
                index = notes.Count + index;
            }

            if (!enabledNotes[index])
            {
//				index = Random.Range(0, availableNotes.Count);
                audioSource.PlayOneShot(availableNotes[Random.Range(0, availableNotes.Count)]);
            }
            else
            {
                audioSource.PlayOneShot(notes[index]);
            }

            //chance of a chord
            if (Random.Range(0, 100) < 30)
            {
                audioSource.PlayOneShot(availableNotes[Random.Range(0, availableNotes.Count)]);
            }

            yield return new WaitForSecondsRealtime(Random.Range(minWait, maxWait));

            prevIndex = index;
        }
    }
}