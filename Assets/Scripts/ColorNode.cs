﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ColorNode : MonoBehaviour {

    public enum NodeType
    {
        Start,
        MainEnd,
        End,
        Normal,
    };

    public NodeType nodeType = NodeType.Normal;
    public LevelManager.GameColor gameColor = LevelManager.GameColor.Black;
    
    private LevelManager.GameColor assignedColour;
    
    private SpriteRenderer spriteRenderer;

    private GameController controller;

    private bool isActive = true;
    // Use this for initialization
    
	void Awake () {
        controller = GameObject.FindObjectOfType<GameController>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        SetColor(gameColor);

        if(nodeType == NodeType.End || nodeType == NodeType.MainEnd)
        {
            Vector3 scale = transform.localScale;
            scale *= 1.7f;
            transform.localScale = scale;
        }
	}

    void OnValidate()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        SetColor(gameColor);
    }

    void OnMouseOver()
    {
        if (!isActive) return;
        if (EventSystem.current.IsPointerOverGameObject()) return;
        
        if (Input.GetMouseButtonDown(0))
        {
            controller.SetNextNode(this, GameController.BlendType.Additive);
        }
        else if (Input.GetMouseButtonDown(1))
        {
            controller.SetNextNode(this, GameController.BlendType.Subtractive);
        }
    }

    public void SetColor(LevelManager.GameColor c)
    {

        var DisplayColor = (LevelManager.Instance) ? LevelManager.Instance.DisplayColor : new Dictionary<LevelManager.GameColor, Color>();
        if(!LevelManager.Instance)
        {
            DisplayColor[LevelManager.GameColor.Yellow] = new Color((255.0f / 255), (218.0f / 255), (11.0f / 255));
            DisplayColor[LevelManager.GameColor.Purple] = new Color((194.0f / 255), (157.0f / 255), (206.0f / 255));
            DisplayColor[LevelManager.GameColor.Orange] = new Color((255.0f / 255), (144.0f / 255), (47.0f / 255));
            DisplayColor[LevelManager.GameColor.Blue] = new Color((0.0f / 255), (228.0f / 255), (245.0f / 255));
            DisplayColor[LevelManager.GameColor.Red] = new Color((255.0f / 255), (92.0f / 255), (95.0f / 255));
            DisplayColor[LevelManager.GameColor.Green] = new Color((11.0f / 255), (255.0f / 255), (181.0f / 255));
            DisplayColor[LevelManager.GameColor.Black] = new Color((52.0f / 255), (52.0f / 255), (52.0f / 255), 0.88f);
            DisplayColor[LevelManager.GameColor.White] = new Color((226.0f / 255), (222.0f / 255), (210.0f / 255));
        }

        assignedColour = c;
        spriteRenderer.color = DisplayColor[c];
    }
    public void SetInactive()
    {
        Debug.Log("Called");
        //Color c = spriteRenderer.color;
        //c.a = 0.1f;
        //spriteRenderer.color = c;


        isActive = false;
    }
    
    public void SetHidden()
    {
        gameObject.SetActive(false);
        isActive = false;
    }

    public LevelManager.GameColor GetAssignedColor() { return assignedColour; }

    public IEnumerator BlendTo(LevelManager.GameColor targetColor, float time)
    {
        var DisplayColor = LevelManager.Instance.DisplayColor;
        Color targeDisplayColor = DisplayColor[targetColor];
        Color currentColor = spriteRenderer.color;
        float timeRemaining = time;
        while(timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
            yield return new WaitForEndOfFrame();

            spriteRenderer.color = Color.Lerp(targeDisplayColor, currentColor, timeRemaining / time);
        }

    }
}
