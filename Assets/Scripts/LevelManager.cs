﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public enum GameColor
    {
        Red,
        Blue,
        Yellow,
        Orange,
        Purple,
        Green,
        Black,
        White,
        Grey1,
        Grey2,
        Grey3,
        Grey4,
        Grey5,
        Grey6
    };

    public Dictionary<GameColor, Color> DisplayColor;

    public Dictionary<GameColor, float> colorPitch;

    public Dictionary<GameColor, bool> isColorCompleted;

    public Dictionary<GameColor, string> colorToSceneName;

    public Dictionary<GameColor, ColorNode[]> endNodeList; //excludes the main end node

    public static LevelManager Instance
    {
        get
        {
            if (_instance == null)
            {
                return null;
            }
            return _instance;
        }
    }

    private static LevelManager _instance;

    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);

        _instance = this;

        isColorCompleted = new Dictionary<GameColor, bool>();
        isColorCompleted[GameColor.Yellow] = false;
        isColorCompleted[GameColor.Purple] = false;
        isColorCompleted[GameColor.Orange] = false;
        isColorCompleted[GameColor.Blue] = false;
        isColorCompleted[GameColor.Red] = false;
        isColorCompleted[GameColor.Green] = false;
        isColorCompleted[GameColor.Black] = false;
        isColorCompleted[GameColor.White] = false;
        isColorCompleted[GameColor.Grey1] = true;
        isColorCompleted[GameColor.Grey2] = true;
        isColorCompleted[GameColor.Grey3] = true;
        isColorCompleted[GameColor.Grey4] = true;
        isColorCompleted[GameColor.Grey5] = true;
        isColorCompleted[GameColor.Grey6] = true;
        

        DisplayColor = new Dictionary<GameColor, Color>();
        DisplayColor[GameColor.Yellow] = new Color((255.0f / 255), (218.0f / 255), (11.0f / 255));
        DisplayColor[GameColor.Purple] = new Color((194.0f / 255), (157.0f / 255), (206.0f / 255));
        DisplayColor[GameColor.Orange] = new Color((255.0f / 255), (144.0f / 255), (47.0f / 255));
        DisplayColor[GameColor.Blue] = new Color((0.0f / 255), (228.0f / 255), (245.0f / 255));
        DisplayColor[GameColor.Red] = new Color((255.0f / 255), (92.0f / 255), (95.0f / 255));
        DisplayColor[GameColor.Green] = new Color((11.0f / 255), (255.0f / 255), (181.0f / 255));
        DisplayColor[GameColor.Black] = new Color((52.0f / 255), (52.0f / 255), (52.0f / 255), 0.88f);
        DisplayColor[GameColor.White] = new Color((255.0f / 255), (255.0f / 255), (255.0f / 255));
        //for special end scene
        DisplayColor[GameColor.Grey1] = new Color((10.0f / 255), (10.0f / 255), (10.0f / 255));
        DisplayColor[GameColor.Grey2] = new Color((50.0f / 255), (50.0f / 255), (50.0f / 255));
        DisplayColor[GameColor.Grey3] = new Color((90.0f / 255), (90.0f / 255), (90.0f / 255));
        DisplayColor[GameColor.Grey4] = new Color((140.0f / 255), (140.0f / 255), (140.0f / 255));
        DisplayColor[GameColor.Grey5] = new Color((190.0f / 255), (190.0f / 255), (190.0f / 255));
        DisplayColor[GameColor.Grey6] = new Color((210.0f / 255), (210.0f / 255), (210.0f / 255));
        
        colorPitch = new Dictionary<GameColor, float>();
        colorPitch[GameColor.Yellow] = 0.4f;
        colorPitch[GameColor.Purple] = 0.5f;
        colorPitch[GameColor.Orange] = 0.6f;
        colorPitch[GameColor.Blue] = 0.7f;
        colorPitch[GameColor.Red] = 0.8f;
        colorPitch[GameColor.Green] = 0.9f;
        colorPitch[GameColor.Black] = 1;
        colorPitch[GameColor.White] = 1.1f;

        colorToSceneName = new Dictionary<GameColor, string>();
        colorToSceneName[GameColor.Yellow] = "StartingYellow";
        colorToSceneName[GameColor.Purple] = "StartingPurple";
        colorToSceneName[GameColor.Orange] = "StartingOrange";
        colorToSceneName[GameColor.Blue] = "StartingBlue";
        colorToSceneName[GameColor.Red] = "StartingRed";
        colorToSceneName[GameColor.Green] = "StartingGreen";
        colorToSceneName[GameColor.Black] = "StartingBlack";
        colorToSceneName[GameColor.White] = "CreditScreen";
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "TitleScreen" || scene.name == "CreditScreen") return;

        ColorNode[] endNodes = Array.FindAll(FindObjectsOfType<ColorNode>(), x => x.nodeType == ColorNode.NodeType.End);

        endNodeList = new Dictionary<GameColor, ColorNode[]>();

        endNodeList[GameController.Instance.currentNode.gameColor] = endNodes;
        foreach (var endNode in endNodes)
        {
            if (endNode.gameColor == GameColor.Black)
            {
                bool fail = false;
                foreach (GameColor col in Enum.GetValues(typeof(GameColor)))
                {
                    if (!isColorCompleted[col] && (col != GameColor.Black
                        && col != GameColor.Grey1
                        && col != GameColor.Grey2
                        && col != GameColor.Grey3
                        && col != GameColor.Grey4
                        && col != GameColor.Grey5
                        && col != GameColor.Grey6
                        && col != GameColor.White))
                    {
                        fail = true;
                        break;
                    }
                }
                //hide the black node
                if (fail)
                {
                    endNode.SetHidden();
                }
                else
                {
                
                    //only show the black node if all others completed i.e. hide the mainendnode
                    ColorNode mainNode = Array.FindAll(FindObjectsOfType<ColorNode>(), x => x.nodeType == ColorNode.NodeType.MainEnd)[0];
                    mainNode.SetHidden();
                }
            }
            //fade out completed nodes
            else if (isColorCompleted[endNode.gameColor])
            {
                endNode.SetInactive();
            }
        }
    }

    // Use this for initialization
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}