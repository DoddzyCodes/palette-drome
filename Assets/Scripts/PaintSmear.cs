﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintSmear : MonoBehaviour {
    private Material mat;
	// Use this for initialization
	void Awake () {

        GetComponent<SpriteRenderer>().enabled = false;
        mat = GetComponent<SpriteRenderer>().material;
    }

    public void RunPaintSmear(ColorNode start, ColorNode end, float time)
    {
        Color c = start.GetComponent<SpriteRenderer>().color;
        c.a = 0.7f;
        mat.SetColor("_Color", c);
        //Debug.Log(mat.GetColor("_BaseColor"))
        Vector3 offset = end.transform.position - start.transform.position;
        float distance = offset.magnitude;

        Vector3 direction = offset.normalized;
        transform.position = (start.transform.position + (distance * 0.5f * direction));


        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        var scale = transform.localScale;

        scale.x = distance * 0.1f * 0.8f;
        transform.localScale = scale;

        GetComponent<SpriteRenderer>().enabled = true;
        StartCoroutine(DoSmear(time));

    }

    private IEnumerator DoSmear(float trailTime)
    {
        float g = 0;
        while(g < 1.0f)
        {
            g += Time.deltaTime / trailTime;
            mat.SetFloat("_GradientCutoff", g);
            yield return new WaitForEndOfFrame();

        }
    }
}

//17.54 309
//9.26, 4.48