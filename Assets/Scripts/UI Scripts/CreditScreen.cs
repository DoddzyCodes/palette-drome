using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditScreen : MonoBehaviour {

    CanvasFadeInOut fader;
    void Start()
    {

        fader = GetComponent<CanvasFadeInOut>();
    }

	public void OnExitButtonClicked()
    {
        CanvasFadeInOut.FadeCompletedCallback cb = () =>
        {
            Application.Quit();
        };
        StartCoroutine(fader.FadeOut(cb));
    }

    public void OnMenuButtonClicked()
    {
        CanvasFadeInOut.FadeCompletedCallback cb = () =>
        {
            SceneManager.LoadScene("TitleScreen");
        };
        StartCoroutine(fader.FadeOut(cb));
    }
}
