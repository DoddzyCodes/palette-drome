using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour {
    public GameObject PaletteButton;
    public GameObject ChartButton;

    public Palette PalettePanel;
    public ColorChart ChartPanel;
    void Start()
    {
        PalettePanel.gameObject.SetActive(false);


        ChartPanel.gameObject.SetActive(false);
    }

    public void ShowPaletteButton()
    {
        PaletteButton.SetActive(true);
    }

    public void ShowChartButton()
    {
        ChartButton.SetActive(true);
    }

    public void OnChartClicked()
    {
        ChartPanel.Display();
        ChartButton.SetActive(false);
    }

    public void OnPaletteClicked()
    {
        PalettePanel.Display();
        PaletteButton.SetActive(false);
    }

    public void OnResetClicked()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("TitleScreen");
    }

   void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            LoadMenu();
        }
    }
}
