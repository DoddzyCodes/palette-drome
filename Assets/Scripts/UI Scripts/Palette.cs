using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class Palette : MonoBehaviour {

    Dictionary<LevelManager.GameColor, PaletteBlob> BlobImages;
    
	// Use this for initialization
	void Awake () {
        BlobImages = new Dictionary<LevelManager.GameColor, PaletteBlob>();
        BlobImages[LevelManager.GameColor.Blue] = transform.Find("Image_Blue").gameObject.GetComponent<PaletteBlob>();
        BlobImages[LevelManager.GameColor.Purple] = transform.Find("Image_Purple").gameObject.GetComponent<PaletteBlob>();
        BlobImages[LevelManager.GameColor.Red] = transform.Find("Image_Red").gameObject.GetComponent<PaletteBlob>();
        BlobImages[LevelManager.GameColor.Orange] = transform.Find("Image_Orange").gameObject.GetComponent<PaletteBlob>();
        BlobImages[LevelManager.GameColor.Yellow] = transform.Find("Image_Yellow").gameObject.GetComponent<PaletteBlob>();
        BlobImages[LevelManager.GameColor.Green] = transform.Find("Image_Green").gameObject.GetComponent<PaletteBlob>();
        BlobImages[LevelManager.GameColor.Black] = transform.Find("Image_Black").gameObject.GetComponent<PaletteBlob>();
    }
	
    internal void Display()
    {
        gameObject.SetActive(true);
        UpdateBlobColours();
    }

    private void UpdateBlobColours()
    {
        foreach (LevelManager.GameColor color in Enum.GetValues(typeof(LevelManager.GameColor)))
        {
            if(BlobImages.ContainsKey(color) && LevelManager.Instance.isColorCompleted[color])
            {
                BlobImages[color].DisplayColoredBlob();
            }
        }
    }

    public void OnCloseButtonClicked()
    {
        GameUI ui = GetComponentInParent<GameUI>();
        ui.ShowPaletteButton();
        gameObject.SetActive(false);
    }
}
