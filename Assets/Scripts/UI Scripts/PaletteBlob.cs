using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaletteBlob : MonoBehaviour {

    public Sprite SpriteToDisplay;

    public void DisplayColoredBlob()
    {
        Image image = GetComponent<Image>();
        image.sprite = SpriteToDisplay;

        Color c = GetComponent<Image>().color;
        c.a = 1.0f;
        image.color = c;
    }
}
