using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChart : MonoBehaviour {
    public void Display()
    {
        gameObject.SetActive(true);
    }

    public void OnExitButtonClicked()
    {
        gameObject.SetActive(false);
        GetComponentInParent<GameUI>().ShowChartButton();
    }
}
