using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasFadeInOut : MonoBehaviour {
    public List<Graphic> ChildrenToIgnore;
    public float FadeTime = 1.0f;

    public delegate void FadeCompletedCallback();

    // Use this for initialization
    void Start () {
        StartCoroutine(FadeIn(FadeTime));
	}

    public IEnumerator FadeIn(float fadeTime)
    {
        Graphic[] elements = GetComponentsInChildren<Graphic>();
        Dictionary<Graphic, float> originalAlphaValues = new Dictionary<Graphic, float>();
        foreach(Graphic g in elements)
        {
            if (!ChildrenToIgnore.Contains(g))
            {
                originalAlphaValues[g] = g.color.a;
                SetAlpha(g, 0.0f);
            }
        }

        float alpha = 0;
        while(alpha < 1)
        {
            alpha += Time.deltaTime / fadeTime;
            foreach (Graphic g in elements)
            {
                if (!ChildrenToIgnore.Contains(g))
                {
                    SetAlpha(g, (alpha < originalAlphaValues[g] ? alpha : originalAlphaValues[g]));
                }
            }

            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator FadeOut(FadeCompletedCallback callback = null, float fadeTime = 1.0f)
    {
        Graphic[] elements = GetComponentsInChildren<Graphic>();
        float alpha = 1.0f;
        while (alpha > 0)
        {
            alpha -= Time.deltaTime / fadeTime;
            foreach (Graphic g in elements)
            {
                if (!ChildrenToIgnore.Contains(g))
                {
                    SetAlpha(g, (alpha < g.color.a ? alpha : g.color.a));
                }
            }

            yield return new WaitForEndOfFrame();
        }

        if (callback != null) callback();
    }

    private void SetAlpha(Graphic g, float v)
    {
        Color c = g.color;
        c.a = v;
        g.color = c;
    }
}
