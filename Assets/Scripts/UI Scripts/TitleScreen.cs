using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour {


    private CanvasFadeInOut fader;
    void Start()
    {
        if(LevelManager.Instance)
        {
            Destroy(LevelManager.Instance.gameObject);
            Debug.Log("Destroyed Level Manager");
        }

        fader = GetComponent<CanvasFadeInOut>();
    }

	public void OnExitButtonClicked()
    {
        CanvasFadeInOut.FadeCompletedCallback cb = () =>
        {
            Application.Quit();
        };
        StartCoroutine(fader.FadeOut(cb));
    }
    
    public void OnHowToPlayButtonClicked()
    {
        CanvasFadeInOut.FadeCompletedCallback cb = () =>
        {
            SceneManager.LoadScene("HowToPlayScreen");
        };
        StartCoroutine(fader.FadeOut(cb));
    }

    public void OnCreditButtonClicked()
    {
        CanvasFadeInOut.FadeCompletedCallback cb = () =>
        {
            SceneManager.LoadScene("CreditScreen");
        };
        StartCoroutine(fader.FadeOut(cb));
    }

    public void OnGreenButtonClicked()
    {
        CreateLevelManager();
        SceneManager.LoadScene("StartingGreen");
        StartCoroutine(fader.FadeOut());
    }

    public void OnOrangeButtonClicked()
    {
        CreateLevelManager();
        StartCoroutine(fader.FadeOut());
        SceneManager.LoadScene("StartingOrange");
    }

    public void OnRedButtonClicked()
    {
        CreateLevelManager();
        StartCoroutine(fader.FadeOut());
        SceneManager.LoadScene("StartingRed");
    }

    public void OnYellowButtonClicked()
    {
        CreateLevelManager();
        StartCoroutine(fader.FadeOut());
        SceneManager.LoadScene("StartingYellow");
    }

    public void OnBlueButtonClicked()
    {
        CreateLevelManager();
        StartCoroutine(fader.FadeOut());
        SceneManager.LoadScene("StartingBlue");
    }

    public void OnPurpleButtonClicked()
    {
        CreateLevelManager();
        StartCoroutine(fader.FadeOut());
        SceneManager.LoadScene("StartingPurple");
    }

    private void CreateLevelManager()
    {
        GameObject go = new GameObject("Level Manager");
        go.AddComponent<LevelManager>();
    }
}
