﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using TransitionDictionary = System.Collections.Generic.Dictionary<LevelManager.GameColor,
    System.Collections.Generic.Dictionary<LevelManager.GameColor, LevelManager.GameColor>>;

public class GameController : MonoBehaviour
{
    public enum BlendType
    {
        Additive,
        Subtractive
    };


    public static GameController Instance
    {
        get
        {
            if (_instance == null)
            {
                return null;
            }
            return _instance;
        }
    }

    private static GameController _instance;

    public Texture2D normalCursor;
    public Texture2D failCursor;

    public int NumberOfClicksBeforeHelp = 5;
    public GameObject PaintSmearPrefab;

    public ColorNode currentNode;
    public LevelManager.GameColor currentColor;
   
    public TransitionDictionary colorTransitionsAdd;
    public TransitionDictionary colorTransitionsSub;

    public AudioClip errorSound; //when clicking somewhere you can't move
    public AudioClip addSound; //sound when arrive at node after adding
    public AudioClip subtractSound; //sound when arrive at node after subtracting
//    public AudioClip travelSound; //can shift the pitch of this up/down for add/subtract as it moves

    private BlendType lastBlendType;

    private float pathTime = 2f;

    //these two keep record of sound events to replay later
    public List<AudioClip> soundHistory;

    public List<float> timeHistory;

    private AudioSource audioSource; //add travel sound to this, on loop


    private SwirlController swirl;

    private bool currentlyMovingBetweenNodes = false;


    private GameObject helpButton;
    private int incorrectClicks = 0;
    void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }

        SetupTransitionTables();

        ColorNode[] startNodeList = Array.FindAll(GameObject.FindObjectsOfType<ColorNode>(),
            x => x.nodeType == ColorNode.NodeType.Start);
        Debug.Assert(startNodeList.Length == 1, "Should only be one start node in the scene!");
        currentNode = startNodeList[0];

        ColorNode[] mainEndNodeList = Array.FindAll(GameObject.FindObjectsOfType<ColorNode>(),
            x => x.nodeType == ColorNode.NodeType.MainEnd);

        Debug.Assert(mainEndNodeList.Length == 1, "Must be at least one end main node in the scene!");

        currentColor = currentNode.gameColor;
        Vector3 trailStartPosition = currentNode.transform.position;
        trailStartPosition.z = -5;

        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;

        swirl = GameObject.FindObjectOfType<SwirlController>();
        Vector3 swirlPos = currentNode.transform.position;
        swirlPos.z = 0.2f;
        swirl.transform.position = swirlPos;

        currentlyMovingBetweenNodes = false;

        helpButton = GameObject.Find("Button_chart");
        //if (helpButton) helpButton.SetActive(false);
    }

    private void SetupTransitionTables()
    {
        colorTransitionsAdd =
            new Dictionary<LevelManager.GameColor, Dictionary<LevelManager.GameColor, LevelManager.GameColor>>();

        colorTransitionsAdd[LevelManager.GameColor.Red] =
            new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Red][LevelManager.GameColor.Blue] = LevelManager.GameColor.Purple;
        colorTransitionsAdd[LevelManager.GameColor.Red][LevelManager.GameColor.Yellow] = LevelManager.GameColor.Orange;
        colorTransitionsAdd[LevelManager.GameColor.Red][LevelManager.GameColor.Green] = LevelManager.GameColor.Black;

        colorTransitionsAdd[LevelManager.GameColor.Blue] =
            new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Blue][LevelManager.GameColor.Red] = LevelManager.GameColor.Purple;
//        colorTransitionsAdd[LevelManager.GameColor.Blue][LevelManager.GameColor.Yellow] = LevelManager.GameColor.Green;
        colorTransitionsAdd[LevelManager.GameColor.Blue][LevelManager.GameColor.Yellow] = LevelManager.GameColor.Green;
        colorTransitionsAdd[LevelManager.GameColor.Blue][LevelManager.GameColor.Orange] = LevelManager.GameColor.Black;

        colorTransitionsAdd[LevelManager.GameColor.Yellow] =
            new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Yellow][LevelManager.GameColor.Red] = LevelManager.GameColor.Orange;
        colorTransitionsAdd[LevelManager.GameColor.Yellow][LevelManager.GameColor.Blue] = LevelManager.GameColor.Green;
        colorTransitionsAdd[LevelManager.GameColor.Yellow][LevelManager.GameColor.Purple] = LevelManager.GameColor.Black;

        colorTransitionsAdd[LevelManager.GameColor.Orange] =
            new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Orange][LevelManager.GameColor.Blue] = LevelManager.GameColor.Black;

        colorTransitionsAdd[LevelManager.GameColor.Purple] =
            new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Purple][LevelManager.GameColor.Yellow] =
            LevelManager.GameColor.Black;

        colorTransitionsAdd[LevelManager.GameColor.Green] =
            new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Green][LevelManager.GameColor.Red] = LevelManager.GameColor.Black;
        
        //for special end black-to-white level
        colorTransitionsAdd[LevelManager.GameColor.Black] = new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Black][LevelManager.GameColor.White] = LevelManager.GameColor.Grey1;
        colorTransitionsAdd[LevelManager.GameColor.Grey1] = new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Grey1][LevelManager.GameColor.White] = LevelManager.GameColor.Grey2;
        colorTransitionsAdd[LevelManager.GameColor.Grey2] = new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Grey2][LevelManager.GameColor.White] = LevelManager.GameColor.Grey3;
        colorTransitionsAdd[LevelManager.GameColor.Grey3] = new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Grey3][LevelManager.GameColor.White] = LevelManager.GameColor.Grey4;
        colorTransitionsAdd[LevelManager.GameColor.Grey4] = new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Grey4][LevelManager.GameColor.White] = LevelManager.GameColor.Grey5;
        colorTransitionsAdd[LevelManager.GameColor.Grey5] = new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Grey5][LevelManager.GameColor.White] = LevelManager.GameColor.White;
        colorTransitionsAdd[LevelManager.GameColor.Grey6] = new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsAdd[LevelManager.GameColor.Grey6][LevelManager.GameColor.White] = LevelManager.GameColor.White;


        colorTransitionsSub =
            new Dictionary<LevelManager.GameColor, Dictionary<LevelManager.GameColor, LevelManager.GameColor>>();

        colorTransitionsSub[LevelManager.GameColor.Orange] =
            new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsSub[LevelManager.GameColor.Orange][LevelManager.GameColor.Yellow] = LevelManager.GameColor.Red;
        colorTransitionsSub[LevelManager.GameColor.Orange][LevelManager.GameColor.Red] = LevelManager.GameColor.Yellow;

        colorTransitionsSub[LevelManager.GameColor.Purple] =
            new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsSub[LevelManager.GameColor.Purple][LevelManager.GameColor.Red] = LevelManager.GameColor.Blue;
        colorTransitionsSub[LevelManager.GameColor.Purple][LevelManager.GameColor.Blue] = LevelManager.GameColor.Red;

        colorTransitionsSub[LevelManager.GameColor.Green] =
            new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsSub[LevelManager.GameColor.Green][LevelManager.GameColor.Yellow] = LevelManager.GameColor.Blue;
        colorTransitionsSub[LevelManager.GameColor.Green][LevelManager.GameColor.Blue] = LevelManager.GameColor.Yellow;


        colorTransitionsSub[LevelManager.GameColor.Black] =
            new Dictionary<LevelManager.GameColor, LevelManager.GameColor>();
        colorTransitionsSub[LevelManager.GameColor.Black][LevelManager.GameColor.Yellow] =
            LevelManager.GameColor.Purple;
        colorTransitionsSub[LevelManager.GameColor.Black][LevelManager.GameColor.Red] = LevelManager.GameColor.Green;
        colorTransitionsSub[LevelManager.GameColor.Black][LevelManager.GameColor.Blue] = LevelManager.GameColor.Orange;
        colorTransitionsSub[LevelManager.GameColor.Black][LevelManager.GameColor.Purple] =
            LevelManager.GameColor.Yellow;
        colorTransitionsSub[LevelManager.GameColor.Black][LevelManager.GameColor.Green] = LevelManager.GameColor.Red;
        colorTransitionsSub[LevelManager.GameColor.Black][LevelManager.GameColor.Orange] = LevelManager.GameColor.Blue;
    }

    public void SetNextNode(ColorNode nextNode, BlendType type)
    {
        if (currentNode == nextNode)
        {
            Debug.Log("Clicked on yourself");
            if (errorSound)
            {
                audioSource.PlayOneShot(errorSound);
            }
            StartCoroutine(ShowErrorCursor());
            return;
        }

        if ((nextNode.nodeType == ColorNode.NodeType.End || nextNode.nodeType == ColorNode.NodeType.MainEnd) &&
            currentColor != nextNode.GetAssignedColor())
        {
            Debug.Log("can't move to final node");
            if (errorSound)
            {
                audioSource.PlayOneShot(errorSound);
                //                soundHistory.Add(errorSound);
                //                timeHistory.Add(Time.time);
            }
            StartCoroutine(ShowErrorCursor());
            AddIncorrectClick();
            return;
        }
        else if (nextNode.nodeType == ColorNode.NodeType.End || nextNode.nodeType == ColorNode.NodeType.MainEnd)
        {
            type = BlendType.Additive;
        }

        lastBlendType = type;

        TransitionDictionary transition = (type == BlendType.Additive) ? colorTransitionsAdd : colorTransitionsSub;
        if ((nextNode.nodeType != ColorNode.NodeType.End && nextNode.nodeType != ColorNode.NodeType.MainEnd) &&
            (!transition.ContainsKey(currentColor) || !transition[currentColor].ContainsKey(nextNode.gameColor)))
        {
            if (errorSound)
            {
                audioSource.PlayOneShot(errorSound);
                //                soundHistory.Add(errorSound);
                //                timeHistory.Add(Time.time);
            }
            StartCoroutine(ShowErrorCursor());
            AddIncorrectClick();
            return;
        }

        if (currentlyMovingBetweenNodes)
        {
            if (errorSound)
            {
                audioSource.PlayOneShot(errorSound);
                //                soundHistory.Add(errorSound);
                //                timeHistory.Add(Time.time);
            }
            StartCoroutine(ShowErrorCursor());
            return;
        }

        ColorNode prevNode = currentNode;

        currentNode = nextNode;
        if (currentNode.nodeType != ColorNode.NodeType.End && currentNode.nodeType != ColorNode.NodeType.MainEnd)
            currentColor = transition[prevNode.GetAssignedColor()][currentNode.GetAssignedColor()];

        //displayNode.color = currentColor;
        swirl.FadeTo(nextNode.transform.position);
        AddPaintSmear(prevNode, nextNode, pathTime);
        StartCoroutine(ActivateCurrentNode(pathTime));
        StartCoroutine(SetNodeInactiveAfter(prevNode, pathTime));
        //travel sound
        if (audioSource.clip)
        {
            StartCoroutine(PlayTravelSound(LevelManager.Instance.colorPitch[currentNode.gameColor],
                LevelManager.Instance.colorPitch[nextNode.gameColor], pathTime));
        }

        if (currentNode.nodeType == ColorNode.NodeType.End || currentNode.nodeType == ColorNode.NodeType.MainEnd)
        {
            StartCoroutine(DisplayLevelWin(3.0f));
            StartCoroutine(LevelCompleted(nextNode.gameColor));
        }

        currentlyMovingBetweenNodes = true;
    }

    private void AddIncorrectClick()
    {
        incorrectClicks++;
        if(incorrectClicks >= NumberOfClicksBeforeHelp && helpButton)
        {
            helpButton.SetActive(true);
        }
    }

    private IEnumerator ShowErrorCursor()
    {
        Cursor.SetCursor(failCursor, new Vector2(0, 0), CursorMode.Auto);
        yield return new WaitForSeconds(0.5f);
        Cursor.SetCursor(normalCursor, new Vector2(0, 0), CursorMode.Auto);
    }

    private IEnumerator PlayTravelSound(float startPitch, float endPitch, float pathTime)
    {
        //current pitch
        audioSource.pitch = startPitch;
        float diff = endPitch - startPitch;
        float timePassed = 0;
        float divisor = 10f; //todo make sure the 10 is a good number to divide by
        audioSource.loop = true;
        audioSource.Play();
        soundHistory.Add(audioSource.clip);
        timeHistory.Add(Time.time);
        while (timePassed < pathTime)
        {
            yield return new WaitForSecondsRealtime(pathTime / divisor);
            audioSource.pitch += diff;
            timePassed += pathTime / divisor;
        }
        audioSource.loop = false;
//        audioSource.Stop();
    }

    private void AddPaintSmear(ColorNode start, ColorNode end, float time)
    {
        GameObject go = Instantiate(PaintSmearPrefab) as GameObject;
        PaintSmear smear = go.GetComponent<PaintSmear>();
        smear.RunPaintSmear(start, end, pathTime);
    }

    private IEnumerator DisplayLevelWin(float v)
    {
        yield return new WaitForSeconds(v);
        Debug.Log("Okay, you win" + currentColor);
    }
   
    private IEnumerator SetNodeInactiveAfter(ColorNode prevNode, float time)
    {
        yield return new WaitForSeconds(time);
        prevNode.SetInactive();
    }

    private IEnumerator ActivateCurrentNode(float time)
    {
        StartCoroutine(currentNode.BlendTo(currentColor, time));
        yield return new WaitForSeconds(time);
        if (lastBlendType == BlendType.Additive)
        {
            if (addSound)
            {
                audioSource.PlayOneShot(addSound);
                soundHistory.Add(addSound);
                timeHistory.Add(Time.time);
            }
        }
        else if (lastBlendType == BlendType.Subtractive)
        {
            if (subtractSound)
            {
                audioSource.PlayOneShot(subtractSound);
                soundHistory.Add(subtractSound);
                timeHistory.Add(Time.time);
            }
        }

        currentNode.SetColor(currentColor);
        currentlyMovingBetweenNodes = false;
    }

    public IEnumerator LevelCompleted(LevelManager.GameColor nextColorToStartOn)
    {
        LevelManager.Instance.isColorCompleted[currentNode.gameColor] = true;
        yield return new WaitForSecondsRealtime(2f);
        //stop music before doing replay
        //todo fade out music
        MusicManager.Instance.audioSource.Stop();
//        StartCoroutine(ReplayPath());
        yield return new WaitForSecondsRealtime(2f);
        SceneManager.LoadScene(LevelManager.Instance.colorToSceneName[nextColorToStartOn]);
    }

    public IEnumerator ReplayPath()
    {
        //todo replay sound path simultaneous with movement path (but faster for both)
        float timePassed = 0;
        float timeGap = 0.05f;
        float timeMultiplier = 2f;
        int i = 0;
        while (i < timeHistory.Count)
        {
            if (timePassed >= timeHistory[i])
            {
//                if (soundHistory[i] == audioSource.clip)
//                {
//                    //        audioSource.pitch = startPitch; //todo
//                    //        float diff = endPitch - startPitch; //todo
//                    float newTimePassed = 0;
//                    float divisor = 50f;
//                    while (newTimePassed < pathTime / timeMultiplier)
//                    {
//                        if (!audioSource.isPlaying)
//                        {
//                            audioSource.Play();
//                        }
//                        yield return new WaitForSecondsRealtime(pathTime / divisor);
//                    //            audioSource.pitch += diff / divisor;
//                        newTimePassed += pathTime / divisor;
//                    }
//                    audioSource.Stop();
//                    timePassed += newTimePassed;
//                }
//                else
//                {
//                    audioSource.PlayOneShot(soundHistory[i]);
//                }
                audioSource.PlayOneShot(soundHistory[i]);
                i++;
            }
            yield return new WaitForSecondsRealtime(timeGap / timeMultiplier);
            timePassed += timeGap;
            Debug.Log("time passed = "+timePassed);
        }
    }
}