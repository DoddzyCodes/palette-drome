﻿Shader "Unlit/PaintSwirl"
{
	Properties
	{
		[PerRendererData]	_MainTex("Sprite Texture", 2D) = "white" {}
							_PerlinTex("Perlin Texture", 2D) = "white" {}
							_Color("Tint", Color) = (1,1,1,1)
		[MaterialToggle]	PixelSnap("Pixel snap", Float) = 0
							_RotationSpeed("Rotation Speed", Float) = 0
	}

	SubShader
	{		
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"

			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord  : TEXCOORD0;
				float2 rotatedtexcoord : TEXCOORD1;
			};

			fixed4 _Color;
			float _RotationSpeed;

			v2f vert(appdata_t IN)
			{

				float sinX = sin(_RotationSpeed * _Time);
				float cosX = cos(_RotationSpeed * _Time);
				float sinY = sin(_RotationSpeed * _Time);
				float2x2 rotationMatrix = float2x2(cosX, -sinX, sinY, cosX);

				float2 rotatedUV = IN.texcoord.xy;
				rotatedUV -= 0.5f;
				rotatedUV = mul(rotatedUV, rotationMatrix);
				rotatedUV += 0.5f;

				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;

				//rotatedUV -= 0.5f;
				//rotatedUV = mul(rotatedUV, rotationMatrix);
				//rotatedUV += 0.5f;

				OUT.rotatedtexcoord = rotatedUV;
				OUT.color = IN.color * _Color;
			
			#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap(OUT.vertex);
			#endif

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _PerlinTex;
			sampler2D _AlphaTex;
			float _AlphaSplitEnabled;

			fixed4 SampleSpriteTexture(float2 uv)
			{
				fixed4 color = tex2D(_MainTex, uv);

				#if UNITY_TEXTURE_ALPHASPLIT_ALLOWED
				if (_AlphaSplitEnabled)
					color.a = tex2D(_AlphaTex, uv).r;
				#endif //UNITY_TEXTURE_ALPHASPLIT_ALLOWED

				return color;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = SampleSpriteTexture(IN.texcoord) * IN.color;
				

				float p = tex2D(_PerlinTex, IN.rotatedtexcoord).r;

				//c.r = p;
				//c.g = 0;
				//c.b = 0;
				//c.a *= p * 1.0f;
				//c.rgb = float3(0, 0, 0);
				if (p > 0.48) c.a -= p;
				//c.r = p;
				//c.g = p;
				//c.b = p;
				//c.a = 1;
				
				c.rgb *= c.a * 1.5f;
				return c;
			}
			ENDCG
		}
	}
}
